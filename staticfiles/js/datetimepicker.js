$(document).ready(function () {
    $("#datetimepicker_organisasi_dateStart").datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $("#datetimepicker_organisasi_dateEnd").datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $("#datetimepicker_prestasi_dateAcquired").datetimepicker({
        format: 'DD/MM/YYYY',
    });
  });