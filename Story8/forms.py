from django import forms
from Story8.models import Search

class SearchForm(forms.ModelForm):
    class Meta:
        model = Search
        fields = [
            'search'
            ]