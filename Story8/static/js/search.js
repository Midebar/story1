$(document).ready(function(){
    $("#id_search").keyup(function(){
        var search = $("#id_search").val();
        $.ajax({
            url:'/Story8/result/?search='+search,
            success: function(data){
                var array_items =  data.items;
                $("#search_result").find("tbody").empty();
                console.log(data.items);
                for(item in data.items){
                    var title = array_items[item].volumeInfo.title;
                    var picture = array_items[item].volumeInfo.imageLinks.smallThumbnail;
                    $("#search_result").find("tbody").append("<tr><td>"+ title +"</td><td>"+"<img src="+picture+"></img></td></tr>");
                }
            }
        })
    })    
});
