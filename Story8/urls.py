from django.urls import path, include
from Story8.views import home, search, result

app_name = "Story8"

urlpatterns = [
    path('', home, name='Home'),
    path('search/', search, name="Search"),
    path('result/', result, name="Result"),
]

