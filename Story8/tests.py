from django.test import TestCase, Client

class TestStory8(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/Story8/')
        self.assertEquals(response.status_code, 200)

    def test_url_googleapis_exist(self):
        response = Client().get('https://www.googleapis.com/books/v1/volumes?q=Tahu')
        self.assertEquals(response.status_code, 200)
    
    def test_url_result_exist(self):
        response = Client().get('/Story8/result/?search=Tahu')
        self.assertEquals(response.status_code, 200)