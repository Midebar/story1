from django.shortcuts import render, redirect
from django.http import JsonResponse
from Story8.forms import SearchForm
import requests, json

def home(request):
    return render(request, "base_story8.html")

def search(request):
    form = SearchForm()
    return render(request, "search_story8.html", {'form': form})

def result(request):
    content = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ request.GET.get('search'))
    data = json.loads(content.content)
    return JsonResponse(data, safe=False)
