from django.contrib import admin
from django.urls import path, include
import Story2.urls, Story1.urls, landing_page.urls, Story7.urls, Story8.urls, Story9.urls
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include(landing_page.urls)),
    path('Story1/', include(Story1.urls)),
    path('Story2/', include(Story2.urls)),
    path('Story7/', include(Story7.urls)),
    path('Story8/', include(Story8.urls)),
    path('Story9/', include(Story9.urls)),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
