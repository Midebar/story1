from django.shortcuts import render, redirect
from django.forms import modelformset_factory
from Story7.models import User, Aktivitas, Organisasi, Prestasi
from Story7.forms import UserForm, AktivitasForm, OrganisasiForm, PrestasiForm
import datetime

def home(request):
    Users = User.objects.all()
    return render(request, "home7.html", {"Users" : Users})
    
# Future project baby
# def details(request):
#     return render(request, "details.html")

def upload(request):
    aktivitasFormSet = modelformset_factory(Aktivitas, form=AktivitasForm, extra = 1)
    organisasiFormSet = modelformset_factory(Organisasi, form=OrganisasiForm, extra = 1)
    prestasiFormSet = modelformset_factory(Prestasi, form=PrestasiForm, extra = 1)
    if request.method == "POST":
        # Using formset to store data
        userFormPost = UserForm(request.POST)
        aktivitasFormPost = aktivitasFormSet(request.POST, queryset=Aktivitas.objects.all(), prefix='aktivitas')
        organisasiFormPost = organisasiFormSet(request.POST, queryset=Organisasi.objects.all(), prefix='organisasi')
        prestasiFormPost = prestasiFormSet(request.POST, queryset=Prestasi.objects.all(), prefix='prestasi')
        print("aktivitas ")
        print(aktivitasFormPost.errors)
        print("organisasi ")
        print(organisasiFormPost.errors)
        print("prestasi ")
        print(prestasiFormPost.errors)
        if userFormPost.is_valid() and aktivitasFormPost.is_valid() and organisasiFormPost.is_valid() and prestasiFormPost.is_valid():
            post_form = userFormPost.save(commit=False)
            post_form.save()
            for aktivitas in aktivitasFormPost.cleaned_data:
                if aktivitas:
                    newAktivitas = Aktivitas(user=post_form, name= aktivitas['name'], desc=aktivitas['desc'])
                    newAktivitas.save()
            for prestasi in prestasiFormPost.cleaned_data:
                if prestasi:
                    newPrestasi = Prestasi(user=post_form, name=prestasi['name'], dateAcquired=(prestasi['dateAcquired']))
                    newPrestasi.save()
            for organisasi in organisasiFormPost.cleaned_data:
                if organisasi:
                    newOrganisasi = Organisasi(user=post_form, name=organisasi['name'], dateStart=organisasi['dateStart'], 
                    dateEnd=organisasi['dateEnd'])
                    newOrganisasi.save()
        return redirect("Story7:Home")
    else:
        userForm = UserForm()
        aktivitasForm = aktivitasFormSet(queryset=Aktivitas.objects.none(), prefix='aktivitas')
        organisasiForm = organisasiFormSet(queryset=Organisasi.objects.none(), prefix='organisasi')
        prestasiForm = prestasiFormSet(queryset=Prestasi.objects.none(), prefix='prestasi')
        return render(request, "upload7.html", {"userForm":userForm, "aktivitasForm":aktivitasForm, "organisasiForm":organisasiForm, "prestasiForm":prestasiForm})
