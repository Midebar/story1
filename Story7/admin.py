from django.contrib import admin
from .models import User, Aktivitas, Organisasi, Prestasi

admin.site.register(User)
admin.site.register(Aktivitas)
admin.site.register(Organisasi)
admin.site.register(Prestasi)