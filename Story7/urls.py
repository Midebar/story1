from django.urls import path, include
from .views import home, upload

app_name = "Story7"

urlpatterns = [
    path('', home, name='Home'),
    path('upload', upload, name="Upload"),
    # path('details/<str:objek>', details, name='Details'),
]

