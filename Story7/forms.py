from django import forms
from Story7.models import User, Aktivitas, Organisasi, Prestasi

class UserForm(forms.ModelForm):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nama kamu'}) , label = 'Nama')
    age = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'Umur kamu'}) , label = 'Umur' )
    class Meta:
        model = User
        fields = ('name', 'age',)

class AktivitasForm(forms.ModelForm):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nama aktivitas'}) , label = 'Aktivitas')
    desc = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'placeholder': 'Deskripsikan aktivitasmu disini'}) , label = 'Deskripsi')
    class Meta:
        model = Aktivitas
        fields = ('name', 'desc',)

# Hard code for date time widget. Idk how to implement object-based widget
class OrganisasiForm(forms.ModelForm):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Organisasi kamu'}) , label = 'Organisasi')
    dateStart = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker_organisasi_dateStart',
        }), label = 'Tanggal Masuk', required=False,
    )
    dateEnd = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker_organisasi_dateEnd',
        }), label = 'Tanggal Keluar', required=False
    )
    class Meta:
        model = Organisasi
        fields = ('name', 'dateStart', 'dateEnd',)

class PrestasiForm(forms.ModelForm):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Prestasi kamu'}) , label = 'Prestasi')
    dateAcquired = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker_prestasi_dateAcquired',
        }), label = 'Tanggal Prestasi', required=False
    )
    class Meta:
        model = Prestasi
        fields = ('name', 'dateAcquired',)
