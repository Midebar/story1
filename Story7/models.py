from django.db import models

class User(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()

class Aktivitas(models.Model):
    name = models.CharField(max_length=50)
    desc = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name="aktivitas")

class Organisasi(models.Model):
    name = models.CharField(max_length=50)
    dateStart = models.DateTimeField()
    dateEnd = models.DateTimeField()
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name="organisasi")

class Prestasi(models.Model):
    name = models.CharField(max_length=50)
    dateAcquired = models.DateTimeField()
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name="prestasi")