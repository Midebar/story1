$(document).ready(function () {
    $('#accordion').accordion({
        collapsible: true,
        active: false,
        height: 'fill',
        header: 'h3'
    }).sortable({
        items: '.sortable'
    });

    $('#accordion').on('accordionactivate', function (event, ui) {
        if (ui.newPanel.length) {
            $('#accordion').sortable('disable');
        } else {
            $('#accordion').sortable('enable');
        }
    });
});