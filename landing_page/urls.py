from django.urls import path, include
from landing_page.views import landing_page

app_name = "landing_page"

urlpatterns = [
   path('', landing_page, name='Home'),
]