from django.test import TestCase, Client
from django.apps import apps
from landing_page.apps import LandingPageConfig
import stories.wsgi

# Create your tests here.

class TestLandingPage(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_app_name(self):
        self.assertEqual(LandingPageConfig.name, 'landing_page')
        self.assertEqual(apps.get_app_config('landing_page').name, 'landing_page')

    def test_wsgi(self):
        self.assertIn('WSGIHandler', str(stories.wsgi.application))