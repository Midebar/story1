from django.urls import path, include
from Story1.views import index

app_name = "Story1"

urlpatterns = [
   path('', index, name='Home'),
]