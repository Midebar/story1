from django.test import TestCase, Client
from Story1.apps import Story1Config
from django.apps import apps

# Create your tests here.
class TestStory1(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/Story1/')
        self.assertEquals(response.status_code, 200)

    def test_url_about_exist(self):
        response = Client().get('/Story1/')
        self.assertTemplateUsed(response, "index.html")

    def test_app_name(self):
        self.assertEqual(Story1Config.name, 'Story1')
        self.assertEqual(apps.get_app_config('Story1').name, 'Story1')