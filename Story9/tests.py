from django.test import TestCase, Client

class TestStory9(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/Story9/')
        self.assertEquals(response.status_code, 200)

    def test_url_signup_exist(self):
        response = Client().get('/Story9/sign_up/')
        self.assertEquals(response.status_code, 200)