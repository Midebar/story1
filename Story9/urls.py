from django.urls import path, include
from Story9.views import home, createUser

app_name = "Story9"

urlpatterns = [
    path('', home, name='Home'),
    path('sign_up/', createUser, name="SignUp")
]

