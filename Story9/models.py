from django.db import models
from django.conf import settings

def get_upload_path(instance, filename):
    model = instance.news.pk
    return f'{model}/images/{filename}'

class Photo(models.Model):
    photo = models.ImageField(upload_to=get_upload_path)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="photo")
