from django.shortcuts import render, redirect
from Story9.forms import  PhotoForm
from Story9.models import Photo
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate

def home(request):
    return render(request, "base_story9.html")

def createUser(request):
    if request.method == "POST":
        userForm = UserCreationForm(request.POST)
        photoForm = PhotoForm(request.POST, request.FILES)
        if userForm.is_valid() and photoForm.is_valid():
            post_form = userForm.save(commit=False)
            post_form.save()
            user = User.objects.create_user(username = userForm.cleaned_data.get('name'),
            email = userForm.cleaned_data.get('email'), password = userForm.cleaned_data('password'))
            photo = Photo(user = post_form, photo = photoForm.cleaned_data.get('photo'))
            photo.save()
        return("Story9:Home")
    else:
        userForm = UserCreationForm()
        photoForm = PhotoForm()
        return render(request, "create_user9.html", {"UserForm": userForm , "PhotoForm": photoForm})

# def login(request):
#     if request.method == "POST":
#     else:
#         return render(request, "base_story9.html")

# def resetPassword(request):
#     if request.method == "POST":
#     else:
#         return render(request, "reset_password.html")

# def logout(request):
#     if request.method == "POST":
#     else:
#         return render(request, "base_story9.html")

# def changeUsername(request):
#     if request.method == "POST":
#     else:
#         return render(request, "change_username.html")

# def changeEmail(request):
#     if request.method == "POST":
#     else:
#         return render(request, "change_email.html")

