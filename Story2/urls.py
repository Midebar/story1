from django.urls import path, include
from Story2.views import home, about, formSave, thanks, details

app_name = "Story2"

urlpatterns = [
   path('', home, name='Home'),
   path('about/', about, name='about'),
   path('forms/', formSave, name='forms'),
   path('thanks/', thanks, name='thanks'),
   path('details/<str:matkul_name>', details, name='details'),
]