from django.db import models

class MatKul(models.Model):
    name = models.CharField(max_length=50)
    sks = models.IntegerField()
    ip = models.IntegerField()
    dosen = models.CharField(max_length=50)
    desc = models.CharField(max_length=100)
    classroom = models.CharField(max_length=10)
    semester = models.DateField()

    def __str__(self):
        return self.name
