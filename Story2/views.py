from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from Story2.forms import MatKulForm
from Story2.models import MatKul

def home(request):
    if (request.method == 'POST'):
        form = MatKulForm(request.POST)
        if(form.is_valid()):
            form.save()
            return redirect("Story2:thanks")
    elif(request.method == "GET"):
        MatKuls = MatKul.objects.all()
        return render(request,"home.html", {'MatKuls':MatKuls})

def about(request):
    return render(request,"about.html")

def deleteAllData(request):
    MatKul.objects.all().delete()
    return redirect("Story2:home")

def formSave(request):
    form = MatKulForm()
    return render(request,'forms.html',{'form':form})

def thanks(request):
    return render(request,"thanks.html")

def details(request, matkul_name):
    try:
        detailMatKul = MatKul.objects.get(name = matkul_name)
    except MatKul.DoesNotExist:
        raise HttpResponse(404, "Mata Kuliah tidak tersedia")
    return render(request, "detailMatKul.html", {"MatKul": detailMatKul})