from django import forms
from Story2.models import MatKul

class MatKulForm(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = ('name', 'sks','ip', 'dosen', 'classroom', 'semester', 'desc')