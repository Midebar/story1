from django.test import TestCase, Client
from Story2.apps import Story2Config
from django.apps import apps
from Story2.models import MatKul
from Story2.forms import MatKulForm
from Story2.views import home
from django.urls import reverse

# Create your tests here.
class TestStory2(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/Story2/')
        self.assertEquals(response.status_code, 200)

    def test_url_about_exist(self):
        response = Client().get('/Story2/about/')
        self.assertTemplateUsed(response, "about.html")
        
    def test_url_thanks_exist(self):
        response = Client().get('/Story2/thanks/')
        self.assertTemplateUsed(response, "thanks.html")

    def test_app_name(self):
        self.assertEqual(Story2Config.name, 'Story2')
        self.assertEqual(apps.get_app_config('Story2').name, 'Story2')

    def test_objects_is_saved(self):
        MatKul.objects.create(name="A", sks=1, ip=1, dosen="AK47", desc ="Pelatihan AK47", classroom ="AK0047", semester = "2019-02-04")
        newMatKul = MatKul.objects.get(name="A")
        self.assertEqual(newMatKul.sks, 1)
        response = Client().get('/Story2/details/A')
        self.assertContains(response, "A", html=True)

    def test_form_working_GET(self):
        form = MatKulForm(data={"name": "B", "sks": 10, "ip" :1, "dosen":"AK47", "desc" :"Pelatihan AK47", "classroom" :"AK0047", "semester" : "2019-02-04"})
        form.save()
        self.assertEqual(MatKul.objects.get(name="B").sks, 10)

    # def test_home_view(self):
    #     response = Client().post('/Story2/home/', {"name": "B", "sks": 10, "ip" :1, "dosen":"AK47", "desc" :"Pelatihan AK47", "classroom" :"AK0047", "semester" : "2019-10-04"})
    #     self.assertContains(response, "B", html=True)